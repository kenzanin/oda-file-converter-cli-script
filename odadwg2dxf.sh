#!/bin/zsh
SAVEIFS=$IFS
SAVEDIR=$PWD
IFS=$(echo -en "\n\b")
DIRLIST=($(find . -mindepth 1 -type d))
DIRLIST_LEN=${#DIRLIST[@]}
for ((i=1;i<=${DIRLIST_LEN};i++))
#for ((i=12;i<=12;i++))
do
	printf "%d/%d entering directory \"%s\" and convert any dwg to dfx\n" $i $DIRLIST_LEN $DIRLIST[i]
	cd $(printf "$DIRLIST[i]")
	echo $PWD
	DWGFILE=($(find ./ -mindepth 1 -maxdepth 1 -type f | grep -i dwg))
	#find ./ -mindepth 1 -maxdepth 1 -type f | grep -i dwg
	DWGFILE_LEN=${#DWGFILE[@]}
	echo ketemu $DWGFILE_LEN files dwg
	
	if [ $DWGFILE_LEN != 0 ]
	then
		
		# bug workaround ganti extension jadi lower case
		RENAME=($(find . -mindepth 1 -maxdepth 1 -type f -name '*.[Dd][Ww][Gg]'))
		RENAME_LEN=${#RENAME[@]}
		for ((ii=1;ii<=$RENAME_LEN;ii++))
		do
			FILE_NAME="${RENAME[ii]%.*}"
			FILE_EXT="${RENAME[ii]##*.}"
			FILE_EXT=($(echo "$FILE_EXT" | tr '[:upper:]' '[:lower:]'))
			if [ ! -f ${FILE_NAME}.${FILE_EXT} ]
			then
				mv "$RENAME[ii]" "${FILE_NAME}.${FILE_EXT}"
			fi
		done
		
		ODA_ARGS=($(printf "./ ./ ACAD2000 DXF 0 1 *.DWG"))
		#echo $ODA_ARGS
		oda-file-converter $ODA_ARGS
	    
		#delete file dwg
		for ((ii=1;ii<=$RENAME_LEN;ii++))
		do
		FILE_NAME="${RENAME[ii]%.*}"
			FILE_EXT="${RENAME[ii]##*.}"
			FILE_EXT=($(echo "$FILE_EXT" | tr '[:upper:]' '[:lower:]'))
 
			if [ -f ${FILE_NAME}.dxf ]
			then
				if [ -f ${FILE_NAME}.${FILE_EXT} ] 
				then 
					rm -f ${FILE_NAME}.${FILE_EXT}
				fi
			fi
		done

	fi
	cd $SAVEDIR
done
IFS=$SAVEIFS
